---
layout: job_family_page
title: "Partner Integration Engineering"
---

The Partner Integration Engineering job family will provide technical guidance to our technology partners through integrating with GitLab using our existing OAuth flows, APIs, and Webhooks. The PIE engineering team will also work closely with GitLab Ecosystem Engineering and GitLab Product Management to advocate for changes in our product to improve the overall integration experience.

## Levels

### Partner Integration Engineer

The Partner Integration Engineer reports to Group Manager, Product.

#### Partner Integration Engineer Job Grade

The Partner Integration Engineer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Partner Integration Engineer Responsbilities

* Provide consultative expertise to partners on strategic integration points on the GitLab platform across all 10 GitLab stages/modules. This may include providing sample scripts or code snippets that demonstrate the best way to integrate with our APIs and Webhooks.
* Advocate for 3rd party ISV and Technology partnerships to provide solution complete portfolio for GitLab customers.
* Lead problem definition and requirements gathering that can frame high-level goals of ambiguous projects and drive reasonable solutions.
* Drive compelling integrated experiences with our partners by gathering technical requirements, supporting design review, receiving feedback, filtering, and distilling partner requests.
* Advocate for partners by working with GitLab Ecosystem Product Management and GitLab Core Engineering to influence the product roadmap to develop features that improve the experience of integrating with GitLab.
* Outline and develop tools, education, and enablement options to help ISV/technology partners build integration with GitLab
* Contribute to the development of assets and knowledge tools for developer.gitlab.com portal.
* Influence a positive outcome in partner relationships and technical projects. 
* Partner with appropriate stakeholders to make ongoing program and project decisions, leverage insights from the team, and make recommendations and/or decisions when issues arise.
* Work with partners and customers to apply modern software development methodologies such as Agile and DevOps on GitLab’s modern devops platform.

#### Partner Integration Engineer Requirements

* Relevant experience as a Partner Engineer, Sales Engineer, Developer Evangelist, or similar roles
* B.A/B.S. or M.S. in Computer Science or equivalent degree or experience
* Experience software in languages like Python, Objective-C, PHP, C/C++, Ruby, C# or other programming languages
* Experience working with GitLab software and GitLab’s core architecture
* Experience working with Web technology stack
* Experience communicating and partnering with cross-functional teams
* Experience developing and effectively managing relationships with external and internal partners
* You have a history of contributing to Open Source projects, and maintain a handful of your own OSS repositories.
* You have modern SDLC experience including:
   * DevSecOps
   * Continuous integration and delivery
   * IAC: Infrastructure as code
   * Application performance monitoring
* You share our values, and work in accordance with those values.
* Ability to use GitLab
* Ability to travel if needed and comply with the company’s [travel policy](https://about.gitlab.com/handbook/travel/) 

## Performance Indicators
(As maintained in SFDC)
* Total number of partners supported, aided, consulted
* Partner integrations launched
* Partner integrations usage
* YoY Partner usage growth

## Career Ladder

The next steps for the Partner Integration Engineering job family is not yet defined.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Hiring Manager
- Next, candidates will be invited to interview with 2-5 team members
- There may be a final executive interview 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
